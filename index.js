const fs = require('fs');
const path = require('path');
const pdf = require('pdf-parse');

const pdfDirectory = path.join(__dirname, 'pdf-files');

console.log(`Kontrollerar filer i: ${pdfDirectory}`);

fs.readdir(pdfDirectory, { encoding: 'utf-8', withFileTypes: true }, (err, files) => {

  console.log(`Hittade, ${files.length}, filer`);

  if (err) throw err;

  files.forEach((file) => {
    fs.readFile(path.join(pdfDirectory, file.name), (err, buffer) => {

      pdf(buffer).then((pdfData) => {
        const pdfMetaData = pdfData.metadata;
        let output;

        // Kolla metadata attribut på pdf:en
        if (pdfMetaData.has('pdfaid:conformance')) {
          output = `${file.name} är en pdf/a`;
          // Gör något med filen, exempelvis flytta den till en pdf/a katalog
        } else {
          output = `${file.name} är inte en pdf/a`;
          // Gör något annat med den här filen... exempelvis kör kommandot för att konvertera den till pdf/a
          // och sedan flytta den till en pdf/a katalog
        }
        // Alternativt skapa en CSV fil med information om varje pdf och om det är pdf/a eller ine som man sedan manuellt kan agera på.
        console.log(output);
      });
    })
  });
});
