# test-pdf
Enkelt NodeJS script som tar en folder med PDF filer och avgör om de är pdf/a eller inte.

## Användning
* Klona git projektet lokalt och kör `npm install`
* Lägg PDF filer i underbiblioteket 'pdf-files' (det finns redan test filer att prova på)
* Kör `node index.js`

## Krav
* Installera [NodeJS](http://www.nodejs.org)